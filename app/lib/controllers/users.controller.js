angular.module(window.nameSpaceApp)
    .controller('UsersController', ['$scope', '$http','$templateCache','DTOptionsBuilder', 'DTColumnBuilder',UsersController]);

function UsersController( $scope, $http,$templateCache,DTOptionsBuilder, DTColumnBuilder  )
{


   $templateCache.removeAll();

    var usr = this;


    /*DSO 
      Datos del panel administrativo.
   */
    usr.panelHead="Inventario de existencias";
    usr.panelDescription="En este panel usted podra administrar los registros de su tienda";
    usr.tableHead="Lista de usuarios registrados";

 
/*DSO 
   Cabeceras de la tabla.
   */
    usr.Theads=[ 
                  { 
                    name:'Avatar'
                  },
                  { 
                    name:'Nombres'
                  },
                  {
                    name:'Apellidos'
                  },
                  {
                    name:'E-mail'
                  },
                  {
                   name:'Telefono'
                  },{
                    name:'Acciones'
                  }
     ];
   

   /*DSO 
    Leer los datos del json y imprimirlos en la tabla.

   */
    $http({
        method: "GET",
        url: 'http://test.alquimedia.co/rest/user',
    }).success(function (data, status, headers, config) {   
      
        usr.DataUsers=data;
         console.log(data);
    }).error(function (data, status, headers, config) {
        alert("Error. Int\u00E9ntalo de nuevo." + status);
    });


  /*DSO 
   Función la cual se encarga de crear los usuarios desde el formulario.

   */
    usr.msj_incorecto = true;
    usr.msj_correcto = true;
    usr.crearUsuario=function(){

        $http({
        method: "POST",
        url: 'http://test.alquimedia.co/register-user',
        data:{
        first_name:usr.nombres,
        last_name:usr.apellidos,
        full_name:usr.nombres+" "+usr.apellidos,
        telephone:usr.telefono,
        email:usr.email
}
    }).success(function (data, status, headers, config) {   
      usr.mensaje="Usuario Creado Correctamente";
              usr.msj_correcto = true;
        // console.log(data);
    }).error(function (data, status, headers, config) {
       usr.mensaje="Se presentó un error al crear el usuario, por favor intentelo de nuevo más tarde.";
         usr.msj_incorecto = false;
        
    });
    }

    /*DSP
    Metodo Eliminar Usuario

    */
    usr.EliminarUsuario=function(id){
       $http({
        method: "DELETE",
        url: 'http://test.alquimedia.co/delete-user',
        data:{ uuid:id }
    }).success(function (data, status, headers, config) {   
     
        console.log(data);
    }).error(function (data, status, headers, config) {
         console.log(data + status);
    });
          
    }


    /*DS
      Metodo Eliminar
    */
       usr.ModificarUsuario=function(id){
       $http({
        method: "PUT",
        url: 'http://test.alquimedia.co/update-user',
        data:{ uuid:id }
    }).success(function (data, status, headers, config) {   
     
        console.log(data);
    }).error(function (data, status, headers, config) {
         console.log(data + status);
    });
          
    }

}